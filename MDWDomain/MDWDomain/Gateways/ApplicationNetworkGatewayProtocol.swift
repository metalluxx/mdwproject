//
//  NetworkGatewayProtocol.swift
//  MDWDomain
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import Foundation

public typealias ShortMaterialHandler = (_ key:[MDWShortMaterial]?, _ error:String?)->()
public typealias DownloadImageHandler = (_ image:UIImage?, _ error:String?)->()
public typealias FullMaterialHandler = (_ fullMaterial:MDWMaterial?, _ error:String?)->()
public typealias PostFormHandler = (_ response:URLResponse? , _ error:String?)->()

public protocol ApplicationNetworkGatewayProtocol {
    func downloadShortMaterials( completion:@escaping ShortMaterialHandler)
    func downloadFullMaterial(shortMaterial:MDWShortMaterial,completion:@escaping FullMaterialHandler)
    func postForm(_ form:MDWFormiqueForm,token:MDWAuthToken,completion:@escaping PostFormHandler)
}
