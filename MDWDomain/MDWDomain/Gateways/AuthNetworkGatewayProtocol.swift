//
//  Auth.swift
//  MDWDomain
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public typealias AuthKeyHandler = (_ key:MDWAuthKey?, _ error:String?)->()
public typealias AuthTokenHandler = (_ key:MDWAuthToken?, _ error:String?)->()

public protocol AuthNetworkGatewayProtocol {
    func getKey(phone:String, completion:@escaping AuthKeyHandler)
    func getToken(key:MDWAuthKey, code:String, completion:@escaping AuthTokenHandler)
}
