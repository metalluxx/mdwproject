//
//  AuthServiseProtocol.swift
//  MDWDomain
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol AuthServiceProtocol{
    var status:ASAuthStatus {get}
    func getKey(phone:String, handler: @escaping AuthKeyHandler)
    func getToken(code:String, handler: @escaping AuthTokenHandler)
    func resetStatus()
    func setGuestStatus()
}
