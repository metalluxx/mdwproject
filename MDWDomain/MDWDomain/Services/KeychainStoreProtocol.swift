//
//  KeychainStoreProtocol.swift
//  MDWDomain
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol KeychainStoreProtocol {
    func save(token:MDWAuthToken?)
    func loadToken() -> MDWAuthToken?
}
