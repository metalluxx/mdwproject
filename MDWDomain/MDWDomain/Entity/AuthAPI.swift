//
//  AuthAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWAuthKey:Codable{
    public var key:String?
    public var error:MDWErrorResponse?
}

extension MDWAuthKey:ExpressibleByStringLiteral{
    public typealias StringLiteralType = String
    public init(stringLiteral value: MDWAuthKey.StringLiteralType) {
        self.key = value
    }
}

public struct MDWAuthToken:Codable{
    public var success:Bool
    public var token:String?
    public var user:MDWUser?
}
