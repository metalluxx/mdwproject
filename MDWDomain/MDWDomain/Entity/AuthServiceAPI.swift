//
//  AuthServiceAPI.swift
//  MDWDomain
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public enum ASAuthStatus{
    case user(token:MDWAuthToken)
    case waitSmsCode(auth:MDWAuthKey)
    case none
    case guest
}
