//
//  UserAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol MDWUserProtocol{
    var id:String{get set}
    var givenName:String{get set}
    var familyName:String{get set}
    var phone:String{get set}
    var email:String{get set}
}
public struct MDWShortUser:MDWUserProtocol,Codable{
    public var id:String
    public var givenName:String
    public var familyName:String
    public var phone:String
    public var email:String
}

public struct MDWUser:MDWUserProtocol,Codable{
    public var id:String
    public var givenName:String
    public var familyName:String
    public var phone:String
    public var email:String
    
    public var city:String?
    public var age:Int?
    public var education:String?
    public var company:String?
    public var role:String?
}

