//
//  FormiqueForm.swift
//  MDW
//
//  Created by Metalluxx on 09/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWFormiqueForm:Codable{
    public var givenName:String
    public var familyName:String
    public var city:String
    public var age:Int
    public var category = "02"
    public var education:String
    public var images = [String]()
    public var email:String?
    public var comment:String?
    
    public init(givenName:String, familyName:String, city:String, age:Int, category:String, education:String){
        self.givenName = givenName
        self.familyName = familyName
        self.city = city
        self.age = age
        self.category = category
        self.education = education
    }
}

