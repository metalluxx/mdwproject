//
//  ErrorAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWErrorResponse:Codable{
    public var code:Int
    public var name:String?
    public var fields:String?
}
