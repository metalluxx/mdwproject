//
//  MDWMaterialBlock.swift
//  MDW
//
//  Created by Metalluxx on 24/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public enum MDWMaterialBlockType:String,Codable{
    case paragraph, image
}

public struct MDWMaterialBlock:Codable{
    public var type:MDWMaterialBlockType
    public var content:MDWMaterialBlockContent
}

public struct MDWMaterialBlockContent:Codable{
    public var title:String?
    public var text:String?
    public var image:MDWImage?
}
