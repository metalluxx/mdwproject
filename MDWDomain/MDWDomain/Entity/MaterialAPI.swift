//
//  MaterialAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol MDWEntity{
    var id:String{get set}
    var permalink:String {get set}
}

public enum MDWMaterialType:String,Codable{
    case article, gallery, project, competition, event, participant
}

public struct MDWShortMaterial:Codable,MDWEntity{
    public var id:String
    public var permalink:String
    
    public var title:String?
    public var description:String?
    public var cover:MDWImage
    public var type:MDWMaterialType
    public var year:Int
    public var published:String?
}

public struct MDWMaterial:MDWEntity,Codable{
    public var id:String
    public var permalink:String
    
    public var title:String?
    public var description:String?
    public var fullCover:MDWImage
    public var blocks:[MDWMaterialBlock]
    public var type:MDWMaterialType
    public var year:Int
    public var published:String?
}

