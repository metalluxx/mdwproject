//
//  ApiStructes.swift
//  MDW
//
//  Created by Metalluxx on 22/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public struct MDWImage:Codable{
    public var width:Int
    public var height:Int
    public var preview:String
    public var original:String
    public var getOriginalURL:URL?{
        return URL(string:original)
    }
    public var getPreviewImage:UIImage?{
        let editedPreview = preview.replacingOccurrences(of:"data:image/jpeg;base64,", with:"")
        guard let data = Data(base64Encoded:editedPreview) else{
            return nil
        }
        return UIImage(data:data) ?? nil
    }
}




