//
//  JSONCamelCase.swift
//  MDW
//
//  Created by Metalluxx on 28/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import Foundation
import DITranquillity

public protocol JSONCamelCaseDecoderTag {}
public protocol JSONCamelCaseEncoderTag {}

public class JSONCodersDependency : DIPart {
    public static func load(container: DIContainer) {
        container.register(JSONDecoder.init)
            .injection {
                $0.keyDecodingStrategy = .convertFromSnakeCase
            }
            .as(JSONDecoder.self, tag: JSONCamelCaseDecoderTag.self)
            .lifetime(.single)

        container.register(JSONEncoder.init)
            .injection {
                $0.keyEncodingStrategy = .convertToSnakeCase
            }
            .as(JSONEncoder.self, tag: JSONCamelCaseEncoderTag.self)
            .lifetime(.single)
        container.initializeSingletonObjects()
    }
}

