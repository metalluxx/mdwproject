//
//  RouterDI.swift
//  ApplicationService
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import DITranquillity

class RouterDependency: DIPart {
    static func load(container: DIContainer) {
        container.register(Router.init)
            .as(Routable.self)
            .lifetime(.single)
        container.initializeSingletonObjects()
    }
}
