//
//  GlobalContainer.swift
//  ApplicationService
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import DITranquillity

public var globalContainer = DIContainer()
    .append(part: JSONCodersDependency.self)
    .append(part: RouterDependency.self)

