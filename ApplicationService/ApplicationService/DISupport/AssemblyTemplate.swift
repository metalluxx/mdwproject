//
//  AssemblyTemplate.swift
//  ApplicationService
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import DITranquillity

public protocol AssemblyTemplate {
    associatedtype OutputObject:Any
    init()
    var container:DIContainer {get}
    func build() -> OutputObject
}
