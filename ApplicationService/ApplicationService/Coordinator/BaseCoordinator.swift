//
//  BaseCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

open class BaseCoordinator:Coordinatable{
    open func start(){
        fatalError("This method is dont overrided")
    }
    public var finishFlow:( ()->Void )?
    
    public let router:Routable
    public private(set) var childCoordinators: [Coordinatable] = []
    public private(set) weak var parentCoordinator:BaseCoordinator?

    public init(router:Routable){
        self.router = router
    }
    
    public convenience init(router:Routable, parent:BaseCoordinator?){
        self.init(router:router)
        parentCoordinator = parent
    }

    public func addDependency(_ coordinator:Coordinatable){
        for element in childCoordinators{
            if element === coordinator{ return }
        }
        childCoordinators.append(coordinator)
    }
    
    public func removeDependency(_ coordinator: Coordinatable?){
        guard
            childCoordinators.isEmpty == false,
            let coordinator = coordinator
            else { return }
        
        for (index, element) in childCoordinators.enumerated(){
            if element === coordinator{
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}
