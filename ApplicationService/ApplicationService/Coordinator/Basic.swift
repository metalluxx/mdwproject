//
//  Basic.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public typealias CoordinatorViewController = UIViewController & CoordinatorManager

public protocol Coordinatable:class{
    func start()
    var finishFlow:( ()->Void )? {get set}
}
public protocol Presentable{
    /// Weak reference to view for present from coordinator
    var currentPresentor:CoordinatorViewController?{ get set }
}

