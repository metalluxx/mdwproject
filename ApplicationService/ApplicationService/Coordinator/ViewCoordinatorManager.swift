//
//  ViewCoordinatorManager.swift
//  MDW
//
//  Created by Metalluxx on 16/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol CoordinatorManager where Self:UIViewController{
    /// Weak reference to parent coordinator for view
    var manageCoordinator:BaseCoordinator?{get set}
}
