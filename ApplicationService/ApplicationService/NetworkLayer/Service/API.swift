//
//  EndPoint.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol APITemplate{
    var baseURL: URL {get}
    var path: String {get}
    var task: HTTPTask {get}
    var httpMethod: HTTPMethod {get}
    var headers: HTTPHeaders? {get}
}

public typealias HTTPHeaders = [String:String]
public typealias Parameters = [String:Any]

public enum HTTPMethod: String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
    case none = ""
}

public enum NetworkError: String, Error {
    case parametersNil
    case encodingFailed
    case missingURL
}

public enum HTTPTask{
    var isDownloadTask: Bool {
        switch self {
        case .requestDownloadFile:
            return true
        default:
            return false
        }
    }
    
    case requestDownloadFile
    case request
    case requestParameters(bodyParameters:Parameters?, urlParameters:Parameters?)
    case requestParametersAndHeaders(bodyParameters:Parameters?, urlParameters:Parameters?, additionHeaders:HTTPHeaders?)
    case requestDataParametersAndHeaders(bodyData:Data?, urlParameters:Parameters?, additionHeaders:HTTPHeaders?)
}
