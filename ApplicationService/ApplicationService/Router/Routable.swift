//
//  Routable.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol Routable{
    
//    func present(_ module: Presentable?)
//    func present(_ module: Presentable?, animated: Bool)
    
    func push(_ module: Presentable?)
    func push(_ module: Presentable?, animated: Bool)
    func push(_ module: Presentable?, animated: Bool, completion: ( () -> Void )?)
    
    func popModule()
    func popModule(animated: Bool)
    
//    func dismissModule()
//    func dismissModule(animated: Bool, completion: CompletionBlock?)
    
    func setRootModule(_ module: Presentable?)
    func setRootModule(_ module:Presentable?, transitionOption:UIView.AnimationOptions)
}
