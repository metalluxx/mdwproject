//
//  Router.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit

public class Router:Routable{
    
    private var navigationController = UINavigationController()
    private var window:UIWindow!
    
    init(){
        navigationController.isNavigationBarHidden = true
        window = UIWindow(frame:UIScreen.main.bounds)
        window.backgroundColor = .white
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    public func setRootModule(_ module:Presentable?){
        guard let toVC = module?.currentPresentor else{return}
        guard let fromVC = navigationController.viewControllers.last else{
            navigationController.setViewControllers([toVC], animated: false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated: false)
        UIWindow.transition(from:fromVC.view, to:toVC.view, duration:0.6, options:[.transitionFlipFromLeft, .curveEaseInOut])
    }
    
    public func setRootModule(_ module:Presentable?, transitionOption:UIView.AnimationOptions){
        guard let toVC = module?.currentPresentor else{return}
        guard let fromVC = navigationController.viewControllers.last else{
            navigationController.setViewControllers([toVC], animated:false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated:false)
        UIWindow.transition(from: fromVC.view, to: toVC.view, duration: 0.6, options: transitionOption)
    }
    
    public func push(_ module:Presentable?){
        push(module, animated:true, completion:nil)
    }
    
    public func push(_ module:Presentable?, animated:Bool){
        push(module, animated: animated, completion: nil)
    }
    
    public func push(_ module: Presentable?, animated:Bool, completion:( ()->Void )?){
        guard let viewController = module?.currentPresentor else{return}
        navigationController.pushViewController(viewController, animated:animated)
        completion?()
    }
    
    public func popModule(){
        popModule(animated: true)
    }
    
    public func popModule(animated:Bool){
        navigationController.popViewController(animated:animated)
    }
    
//    func dismissModule(){
//        dismissModule(animated: true, completion: nil)
//    }
//
//    func dismissModule(animated: Bool, completion: CompletionBlock?){
//        window.rootViewController?.dismiss(animated: animated, completion: completion)
//    }
//
//    func present(_ module: Presentable?){
//        present(module, animated: true)
//    }
//
//    func present(_ module: Presentable?, animated: Bool){
//        guard let viewController = module?.currentPresentor else{return}
//        window.rootViewController?.present(viewController, animated: animated, completion:{
//
//        })
//    }
}
