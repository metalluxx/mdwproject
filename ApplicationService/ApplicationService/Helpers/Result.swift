//
//  Result.swift
//  ApplicationService
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public enum Result<String>{
    case success
    case failure(String)
}
