//
//  AuthServiceDependency.swift
//  MDWDataProvider
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import MDWDomain
import DITranquillity

public class AuthServiceDependency:DIPart{
    public static func load(container: DIContainer) {
        container.append(part: KeychainStoreDependency.self).append(part: NetworkGatewayDependency.self)
        container.register(AuthService.init).injection({
            $0.gateway = *container
            $0.keychainStore = *container
        }).as(AuthServiceProtocol.self).lifetime(.single)
        container.initializeSingletonObjects()
    }

}
