//
//  KeychainStore.swift
//  MDWDataProvider
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import ApplicationService
import MDWDomain


public struct KeychainStore:KeychainStoreProtocol {
    static private let Store = KeychainPasswordItem(service: "MX.testMDW", account: "MDWAccount")
    public init(){}
    public func save(token: MDWAuthToken?) {
        do {
            if let stringToken =  String(data: try JSONEncoder().encode(token), encoding: .utf8){
                try KeychainStore.Store.savePassword(stringToken)
            }
            else{ try KeychainStore.Store.savePassword("") }
        }
        catch { debugPrint(error) }
    }
    
    public func loadToken() -> MDWAuthToken? {
        do {
            guard let dataToken = try KeychainStore.Store.readPassword().data(using: .utf8) else{ return nil }
            let token = try JSONDecoder().decode(MDWAuthToken.self, from: dataToken)
            return token
        }
        catch{
            debugPrint(error)
            return nil
        }
    }
}
