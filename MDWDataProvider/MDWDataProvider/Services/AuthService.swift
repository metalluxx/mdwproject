//
//  AuthServise.swift
//  MDWDataProvider
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import MDWDomain

public class AuthService:AuthServiceProtocol{
    
    public private(set) var status = ASAuthStatus.none{
        didSet{
            guard case let ASAuthStatus.user(token) = status else {
                keychainStore?.save(token: nil)
                return
            }
            keychainStore?.save(token: token)
        }
    }
    
    public var gateway:AuthNetworkGatewayProtocol?
    public var keychainStore:KeychainStoreProtocol?{
        didSet{
            if let token = keychainStore?.loadToken(), token.success {
                status = .user(token: token)
            }
        }
    }
    
    public init(){}
    
    public func getKey(phone: String, handler: @escaping (MDWAuthKey?, String?) -> ()) {
        status = .none
        guard let gateway = gateway else {
            handler(nil,"Dont have gateway in authService")
            return
        }
        
        gateway.getKey(phone: phone) { (key, err) in
            guard
                let key = key,
                key.key != nil
            else {
                handler(nil,err)
                return
            }
            self.status = .waitSmsCode(auth: key)
            handler(key,err)
        }
    }
    
    public func getToken(code:String, handler: @escaping (MDWAuthToken?, String?) -> ()) {
        guard case let ASAuthStatus.waitSmsCode(auth) = status else {
            handler(nil, "You did not enter the phone number")
            return
        }
        guard let gateway = gateway else {
            handler(nil,"Dont have gateway in authService")
            return
        }
        gateway.getToken(key: auth, code: code) { (token, err) in
            guard let correctToken = token, correctToken.success else {
                handler(token,err)
                return
            }
            self.status = .user(token: correctToken)
            handler(token,err)
        }
    }
    
    public func resetStatus() {
        status = .none
        keychainStore?.save(token: nil)
    }
    
    public func setGuestStatus() {
        status = .guest
    }
}
