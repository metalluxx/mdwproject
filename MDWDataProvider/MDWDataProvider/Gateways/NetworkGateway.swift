//
//  NetworkGateway.swift
//  MDWDataProvider
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import ApplicationService
import MDWDomain

public class NetworkGateway:ApplicationNetworkGatewayProtocol,AuthNetworkGatewayProtocol{
    private let router = NetRouter<MDWApi>()
    public static let shared = NetworkGateway()
    
    private static var jsonDecoder:JSONDecoder = globalContainer.resolve(tag: JSONCamelCaseDecoderTag.self)
    
    public init(){}
    
    public func postForm(_ form: MDWFormiqueForm, token: MDWAuthToken, completion: @escaping PostFormHandler) {
        router.dataRequest(MDWApi.sendForm(token:token, form:form)){ (data, response, error) in
            if error != nil{
                completion(response ,error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else{ return }
            let result = self.handleNetworkResponse(response)
            switch result{
            case .success:  completion(response, nil)
            case .failure(let networkFailureError):  completion(nil, networkFailureError)
            }
        }
    }
    
    public func getKey(phone:String, completion:@escaping AuthKeyHandler) {
        router.dataRequest(MDWApi.getAuth(phone:phone)){ (data, response, error) in
            if error != nil{
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else{ return }
            let result = self.handleNetworkResponse(response)
            
            switch result{
            case .success:
                guard let responseData = data else{
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let apiResponse = try NetworkGateway.jsonDecoder.decode(MDWAuthKey.self, from:responseData)
                    completion(apiResponse, nil)
                    return
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                    return
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
                return
            }
        }
    }
    
    public func getToken(key:MDWAuthKey, code:String, completion:@escaping AuthTokenHandler){
        router.dataRequest(MDWApi.getToken(key:key, code:code)){ (data, response, error) in
            if error != nil{
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else{ return }
            let result = self.handleNetworkResponse(response)
            
            switch result{
            case .success:
                guard let responseData = data else{
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let apiResponse = try NetworkGateway.jsonDecoder.decode(MDWAuthToken.self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
                
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    public func downloadShortMaterials( completion:@escaping ShortMaterialHandler){
        router.dataRequest(MDWApi.downloadsShortMaterials){ (data, response, error) in
            if error != nil{
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else{ return }
            let result = self.handleNetworkResponse(response)
            
            switch result{
            case .success:
                guard let responseData = data else{
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let apiResponse = try NetworkGateway.jsonDecoder.decode([MDWShortMaterial].self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }

    
    public func downloadFullMaterial(shortMaterial:MDWShortMaterial,completion:@escaping FullMaterialHandler){
        router.dataRequest(MDWApi.getFullMaterial(shortMaterial:shortMaterial)){ (data, response, error) in
            if error != nil{
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else{ return }
            let result = self.handleNetworkResponse(response)
            
            switch result{
            case .success:
                guard let responseData = data else{
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let apiResponse = try NetworkGateway.jsonDecoder.decode(MDWMaterial.self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    private func handleNetworkResponse(_ response:HTTPURLResponse)->Result<String>{
        switch response.statusCode{
        case 200...299:return .success
        case 401...500:return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599:return .failure(NetworkResponse.badRequest.rawValue)
        case 600:return .failure(NetworkResponse.outdated.rawValue)
        default:return .failure(NetworkResponse.failed.rawValue)
        }
    }
}

