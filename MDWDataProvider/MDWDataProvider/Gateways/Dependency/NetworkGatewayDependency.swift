//
//  AuthServiceDependency.swift
//  MDWDataProvider
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import MDWDomain
import DITranquillity

public class NetworkGatewayDependency:DIPart{
    public static func load(container: DIContainer) {
        container.register(NetworkGateway.init)
            .as(AuthNetworkGatewayProtocol.self)
            .as(ApplicationNetworkGatewayProtocol.self)
            .lifetime(.single)
        container.initializeSingletonObjects()
    }
}
