//
//  ProjectsVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService
import DITranquillity
import MDWDomain
import MDWDataProvider
import MDWViews

class FormiqueVC:CoordinatorViewController,MDWTabBarItemContainer{
    var mdwTabBarItem = MDWTabBarItemField(image:UIImage(named:"tabForm"),
                                           title:NSLocalizedString("FRQ_TB", comment:""))
    weak var manageCoordinator: BaseCoordinator?
    
    let authService:AuthServiceProtocol = *globalContainer
    let appGateway:ApplicationNetworkGatewayProtocol = *globalContainer
    
    private var scrollView = UIScrollView()
    private var navBarView:MDWNavBar!
    
    private var textView = UITextView()
    private var tfGivenName = MDWTextFieldView()
    private var tfFamilyName = MDWTextFieldView()
    private var tfCity = MDWTextFieldView()
    private var tfAge = MDWTextFieldView()
    private var tfEducation = MDWTextFieldView()
    private var buttonSend = UIButton(type:.system)
    
    private var animationController = MDWTextFieldScrollController()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor = .white
        navBarView = MDWNavBar(viewController:self, title:NSLocalizedString("FRQ_TB", comment:""))
        
        guard case ASAuthStatus.user(_) = authService.status else{
            addAcceptLoginView()
            return
        }
        
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = true
        scrollView.keyboardDismissMode = .onDrag
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target:self.view, action:#selector(UIView.endEditing)))

        textView.isEditable = false
        textView.isSelectable = false
        textView.font = MDWConstant.normalFont(ofSize:17)
        textView.text = NSLocalizedString("FRQ_TITLE", comment:"")
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        scrollView.addSubview(textView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            scrollView.leftAnchor.constraint(equalTo:view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo:view.rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            
            textView.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            textView.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
            textView.heightAnchor.constraint(equalToConstant:70),
            textView.topAnchor.constraint(equalTo:scrollView.topAnchor),
            textView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40)
            ])
        
        tfGivenName.fieldText.returnKeyType = .done
        tfGivenName.fieldText.autocorrectionType = .no
        tfGivenName.labelText.text = NSLocalizedString("FRQ_GIVENNAME", comment:"")

        tfFamilyName.fieldText.autocorrectionType = .no
        tfFamilyName.fieldText.returnKeyType = .done
        tfFamilyName.labelText.text = NSLocalizedString("FRQ_FAMILYNAME", comment:"")

        tfCity.fieldText.autocorrectionType = .no
        tfCity.fieldText.returnKeyType = .done
        tfCity.labelText.text = NSLocalizedString("FRQ_CITY", comment:"")

        tfAge.fieldText.autocorrectionType = .no
        tfAge.fieldText.returnKeyType = .done
        tfAge.fieldText.keyboardType = .numberPad
        tfAge.labelText.text = NSLocalizedString("FRQ_AGE", comment:"")

        tfEducation.fieldText.autocorrectionType = .no
        tfEducation.fieldText.returnKeyType = .done
        tfEducation.labelText.text = NSLocalizedString("FRQ_EDUCATION", comment:"")

        buttonSend.addTarget(self, action:#selector(sendForm), for:.touchUpInside)
        buttonSend.backgroundColor = MDWConstant.GrayButtonBackColor
        buttonSend.setTitleColor(UIColor.black, for:.normal)
        buttonSend.titleLabel?.font = MDWConstant.normalFont(ofSize:17)
        buttonSend.setTitle(NSLocalizedString("FRQ_SEND_BUTTON", comment:""), for:.normal)
       
        fillScrollView(textFields:[tfGivenName,tfFamilyName,tfCity,tfAge,tfEducation])
       
        buttonSend.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(buttonSend)
        
        buttonSend.heightAnchor.constraint(equalToConstant:60).isActive = true
        buttonSend.topAnchor.constraint(equalTo:tfEducation.bottomAnchor, constant:20).isActive = true
        buttonSend.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20).isActive = true
        buttonSend.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40).isActive = true
        buttonSend.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20).isActive = true
        
        scrollView.bottomAnchor.constraint(equalTo: buttonSend.bottomAnchor, constant: 30).isActive = true
        
        animationController.returnController = MDWTextFieldReturnController(with:animationController, alwaysReturn:true)
        animationController.contentScrollView = self.scrollView
    }
    
    func fillScrollView(textFields:[MDWTextFieldView]){
        for (index,item) in textFields.enumerated(){
            item.fieldText.enablesReturnKeyAutomatically = false
            item.animationController = animationController
            animationController.textFields.append(item)
            
            item.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(item)
            
            NSLayoutConstraint.activate([
                (index == 0 ?
                    item.topAnchor.constraint(equalTo:textView.bottomAnchor):
                    item.topAnchor.constraint(equalTo:textFields[index - 1].bottomAnchor, constant:20)
                ),
                item.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
                item.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
                item.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40),
                item.heightAnchor.constraint(equalToConstant:60),
                ])
        }
    }
    @objc func sendForm(){
        guard let givenName = tfGivenName.fieldText.text,
            let familyName = tfFamilyName.fieldText.text,
            let city = tfCity.fieldText.text,
            let age = Int(tfAge.fieldText.text ?? "0"),
            let education = tfEducation.fieldText.text else
     {
            DispatchQueue.main.async{
                ErrorAlertNotificationView.showingAlert(in:self.view, info:"Enter all data in form" , type:.error)
            }
            return
        }
        let form = MDWFormiqueForm(givenName:givenName, familyName:familyName,
                                   city:city, age:age,
                                   category:"20", education:education)
        
        guard case let ASAuthStatus.user(token) = authService.status else{return}
        
        appGateway.postForm(form, token:token){ (aResponse, aErr) in
            guard let errorStr = aErr else{return}
            DispatchQueue.main.async{
                ErrorAlertNotificationView.showingAlert(in:self.view, info:errorStr , type:.error)
            }
        }
    }
    
    func addAcceptLoginView(){
        let viewController = AcceptLoginViewController()
        addChild(viewController)
        viewController.didMove(toParent:self)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewController.view)
        NSLayoutConstraint.activate([
            viewController.view.rightAnchor.constraint(equalTo:view.rightAnchor),
            viewController.view.leftAnchor.constraint(equalTo:view.leftAnchor),
            viewController.view.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            viewController.view.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            ])
        viewController.buttonHandler = {
            self.authService.resetStatus()
            self.manageCoordinator?.finishFlow?()
        }
    }
}

extension FormiqueVC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView:UIScrollView){
        navBarView.isSeparated = (scrollView.contentOffset.y > MDWConstant.OffsetForShowingSeparator)
    }
}
