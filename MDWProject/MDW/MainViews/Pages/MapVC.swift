//
//  MapVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import MDWViews

class MapVC:UIViewController,MDWTabBarItemContainer{
    var mdwTabBarItem = MDWTabBarItemField(image:UIImage(named:"tabMap"), title:NSLocalizedString("MAP_TB", comment:""))

    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor = .cyan
    }

}
