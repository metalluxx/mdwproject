//
//  FormiqueTableCell.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import MDWDomain

class ProjectTableCell:UITableViewCell, MDWImageCacheProtocol{
    var cache = NSCache<NSString, UIImage>()
    static var reuseIdentifier = "PROJECT_TABLE_CELL"
    
    private var aTitle = UILabel()
    private var aDescription = UILabel()
    private var aImage = UIImageView()
    private var tapRecognizer:UITapGestureRecognizer!
    
    var titleText:String?{
        set{ aTitle.text = newValue }
        get{ return aTitle.text }
    }
    var descriptionText:String?{
        set{ aDescription.text = newValue }
        get{ return aDescription.text }
    }
    
    var material:MDWShortMaterial?{
        didSet{
            guard let material = material else {return}
            titleText = material.title
            descriptionText = material.description
            aImage.kf.setImage(with: material.cover.getOriginalURL, placeholder: material.cover.getPreviewImage, options: [.transition(.fade(0.5)), .cacheOriginalImage])

        }
    }
    convenience init(material:MDWShortMaterial){
        self.init(frame:CGRect.zero)
        self.material = material
    }
    
    convenience init(style:UITableViewCell.CellStyle, reuseIdentifier:String?, material:MDWShortMaterial){
        self.init(style:style, reuseIdentifier:reuseIdentifier)
        self.material = material
    }
    
    override init(style:UITableViewCell.CellStyle, reuseIdentifier:String?){
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        
        selectionStyle = .none
        layoutMargins = .zero
        separatorInset = .zero
        
        aTitle.font = MDWConstant.boldFont(ofSize:24)
        aDescription.font = MDWConstant.normalFont(ofSize:17)
        aDescription.numberOfLines = 0
        
        aImage.contentMode = .scaleAspectFill
        aImage.clipsToBounds = true
        aImage.image = UIImage(named:"loginImage")
        
        aTitle.translatesAutoresizingMaskIntoConstraints = false
        aDescription.translatesAutoresizingMaskIntoConstraints = false
        aImage.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.insertSubview(aDescription, at: 0)
        contentView.insertSubview(aTitle, at: 0)
        contentView.insertSubview(aImage, at: 0)

        NSLayoutConstraint.activate([
            aDescription.bottomAnchor.constraint(equalTo:contentView.bottomAnchor, constant:-20),
            aDescription.leftAnchor.constraint(equalTo:contentView.leftAnchor, constant:20),
            aDescription.rightAnchor.constraint(equalTo:contentView.rightAnchor, constant:-20),
            aDescription.heightAnchor.constraint(equalToConstant:50),
            
            aTitle.leftAnchor.constraint(equalTo:contentView.leftAnchor, constant:20),
            aTitle.rightAnchor.constraint(equalTo:contentView.rightAnchor, constant:-20),
            aTitle.heightAnchor.constraint(equalToConstant:25),
            aTitle.bottomAnchor.constraint(equalTo:aDescription.topAnchor, constant:-5),

            aImage.leftAnchor.constraint(equalTo:contentView.leftAnchor),
            aImage.rightAnchor.constraint(equalTo:contentView.rightAnchor),
            aImage.topAnchor.constraint(equalTo:contentView.topAnchor, constant:5),
            aImage.bottomAnchor.constraint(equalTo:aTitle.topAnchor, constant:-20),
            ])
    }
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
    override func setSelected(_ selected:Bool, animated:Bool){
        super.setSelected(selected, animated:animated)
    }
}
