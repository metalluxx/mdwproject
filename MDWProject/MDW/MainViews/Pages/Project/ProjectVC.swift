//
//  FormiqueVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService
import MDWViews
import MDWDomain
import MDWDataProvider
import DITranquillity

class ProjectVC:CoordinatorViewController,MDWTabBarItemContainer{
    
    let authService:AuthServiceProtocol = *globalContainer
    let appGateway:ApplicationNetworkGatewayProtocol = *globalContainer
    
    var mdwTabBarItem = MDWTabBarItemField(image:UIImage(named:"tabProj"),
                                           title:NSLocalizedString("PRJ_TB", comment:""))
    weak var manageCoordinator: BaseCoordinator?
    
    var tableView = MDWTableView()
    var refreshControl = UIRefreshControl()
    var tableDataSource = ProjectDataSource()
    
    var errorView:ErrorAlertNotificationView?
    var navBarView:MDWNavBar!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupUI()
        loadNews()
    }
    
    func setupUI(){
        tableView.register(ProjectTableCell.self, forCellReuseIdentifier:ProjectTableCell.reuseIdentifier)
        navBarView = MDWNavBar(viewController:self, title:NSLocalizedString("PRJ_TB", comment:""))
        view.backgroundColor = .white

        refreshControl.addTarget(self, action:#selector(loadNews), for:.valueChanged)
        refreshControl.tintColor = MDWConstant.NavigationBarColors.title
        refreshControl.attributedTitle = NSAttributedString(
            string:NSLocalizedString("PRJ_LOADING_NEWS", comment:""),
            attributes:[.font:MDWConstant.normalFont(ofSize:15)])
        
        tableView.delegate = self
        tableView.dataSource = tableDataSource
        tableView.refreshControl = self.refreshControl
        tableView.allowsSelection = true
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo:view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo:view.rightAnchor),
            ]) 
    }
    @objc func loadNews(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        appGateway.downloadShortMaterials(){ (arrayNews, err) in
            DispatchQueue.main.async{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.refreshControl.endRefreshing()
                guard let unswapArrayNews = arrayNews else{
                    self.errorView?.removeFromSuperview()
                    self.errorView =
                        ErrorAlertNotificationView.showingAlert(in:self.view,
                                                                info:err ?? NSLocalizedString("ERROR", comment:""),
                                                                type:.error)
                    return
                }
                self.tableDataSource.arrayNews = unswapArrayNews
                self.tableView.reloadData()
            }
        }
    }
}

extension ProjectVC:UITableViewDelegate{
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        guard let manageCoordinator = manageCoordinator as? MainViewCoordinator else {
            self.errorView?.removeFromSuperview()
            self.errorView =
                ErrorAlertNotificationView.showingAlert(in:self.view,
                                                        info:"Dont have correct coordinator",
                                                        type:.error)
            return
        }
        let material = tableDataSource.arrayNews[indexPath.row]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        DispatchQueue.main.async{
            self.tableView.allowsSelection = false
        }
        appGateway.downloadFullMaterial(shortMaterial:material){ (fullMaterial, err) in
            DispatchQueue.main.async{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.tableView.allowsSelection = true
                if let error = err{
                    self.errorView?.removeFromSuperview()
                    self.errorView =
                        ErrorAlertNotificationView.showingAlert(in:self.view,
                                                                info:error,
                                                                type:.error)
                    return
                }
                guard let unswapFullMaterial = fullMaterial else{
                    self.errorView?.removeFromSuperview()
                    self.errorView =
                        ErrorAlertNotificationView.showingAlert(in:self.view,
                                                                info:NetworkResponse.unableToDecode.rawValue,
                                                                type:.error)
                    return
                }
                manageCoordinator.showNews(material:unswapFullMaterial)
            }
        }
    }
    func scrollViewDidScroll(_ scrollView:UIScrollView){
        navBarView.isSeparated = (tableView.contentOffset.y > MDWConstant.OffsetForShowingSeparator)
    }
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath)->CGFloat{
        return MDWConstant.ProjectTableCellHeigth
    }
}



