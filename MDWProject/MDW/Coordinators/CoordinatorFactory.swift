//
//  CoordinatorFactory.swift
//  MDW
//
//  Created by Metalluxx on 17/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService
import MDWDomain
import DITranquillity
import MDWDataProvider

struct CoordinatorFactory{
    
    
    static func createNewsCoordinator(parentCoordinator:BaseCoordinator,news:MDWMaterial)->BaseCoordinator&Presentable{
        let coordinator = NewsViewCoordinator(router:parentCoordinator.router,news:news)
        parentCoordinator.addDependency(coordinator)
        coordinator.finishFlow = { [weak coordinator, weak parentCoordinator] in
            parentCoordinator?.removeDependency(coordinator)
        }
        return coordinator
    }
    
    static func createLoginCoordinator(parentCoordinator:BaseCoordinator)->BaseCoordinator&Presentable{
        let coordinator = AuthViewCoordinator(router:parentCoordinator.router)
        parentCoordinator.addDependency(coordinator)
        coordinator.finishFlow = { [weak parentCoordinator, weak coordinator] in
            guard let coordinator = coordinator, let parentCoordinator = parentCoordinator else{return}
            parentCoordinator.removeDependency(coordinator)
            parentCoordinator.start()
        }
        return coordinator
    }
    
    static func createMainCoordinator(parentCoordinator:BaseCoordinator)->BaseCoordinator&Presentable{
        let coordinator = MainViewCoordinator(router:parentCoordinator.router)
        parentCoordinator.addDependency(coordinator)
        coordinator.finishFlow = { [weak parentCoordinator, weak coordinator] in
            guard let coordinator = coordinator, let parentCoordinator = parentCoordinator else{return}
            parentCoordinator.removeDependency(coordinator)
            (globalContainer.resolve() as AuthServiceProtocol).resetStatus()
            parentCoordinator.start()
        }
        return coordinator
    }
}
