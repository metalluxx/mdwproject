//
//  AppCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit
import ApplicationService
import MDWDomain
import MDWDataProvider
import DITranquillity

class AppCoordinator:BaseCoordinator{
    let authService:AuthServiceProtocol = *globalContainer
    
    override func start(){
        switch authService.status{
        case .none:
            showLoginFlow()
        default:
            showMainFlow()
        }
    }
    func showLoginFlow(){
        CoordinatorFactory.createLoginCoordinator(parentCoordinator:self).start()
    }
    func showMainFlow(){
        CoordinatorFactory.createMainCoordinator(parentCoordinator:self).start()
    }
}
