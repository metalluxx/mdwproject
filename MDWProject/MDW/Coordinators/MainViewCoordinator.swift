//
//  MDWTabControllerCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 18/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService
import MDWDomain
import MDWViews

class MainViewCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor: CoordinatorViewController?
    deinit{
        print("Dealloc \(self)")
    }
    override func start(){
            let viewController = MDWTabBarController()
            self.currentPresentor = viewController
            self.currentPresentor?.manageCoordinator = self
            viewController.viewControllers = [ProjectVC(),FormiqueVC(),MapVC(),InfoVC()]
            self.router.setRootModule(self)
    }
    
    func showNews(material:MDWMaterial){
        CoordinatorFactory.createNewsCoordinator(parentCoordinator:self, news:material).start()
    }
}
