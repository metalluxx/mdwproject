//
//  AuthCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService

class AuthViewCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor: CoordinatorViewController?
    deinit{
        print("Dealloc \(self)")
    }
    override func start(){
            let viewController = LoginViewController()
            self.currentPresentor = viewController
            self.currentPresentor?.manageCoordinator = self
            self.router.setRootModule(self,transitionOption:[.transitionFlipFromLeft, .curveEaseInOut])
    }
}

