//
//  NewsViewCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 17/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit
import ApplicationService
import MDWDomain

class NewsViewCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor:CoordinatorViewController?
    deinit{
        print("dealloc \(self)")
    }
    
    var news:MDWMaterial
    required init(router:Routable,news:MDWMaterial){
        self.news = news
        super.init(router: router)
    }
    override func start(){
            let viewController = NewsViewController(material:self.news)
            self.currentPresentor = viewController
            self.currentPresentor?.manageCoordinator = self
            self.router.push(self, animated: true)
    }
}
