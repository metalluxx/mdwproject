//
//  NewsImageParagraph.swift
//  MDW
//
//  Created by Metalluxx on 29/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import MDWDomain
import Kingfisher

class NewsImageParagraph:UITableViewCell, MDWImageCacheProtocol{
    static var reuseIdentifier = "NEWS_IMAGE_CELL"
    var cache = NSCache<NSString, UIImage>()
    
    private var imageParagraph:UIImageView!
    var imageCover:MDWImage?
    
    override init(style:UITableViewCell.CellStyle, reuseIdentifier:String?){
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        self.selectionStyle = .none
        imageParagraph = UIImageView(image:imageCover?.getPreviewImage)
        
        imageParagraph.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageParagraph)
        
        NSLayoutConstraint.activate([
                imageParagraph.topAnchor.constraint(equalTo:contentView.topAnchor),
                imageParagraph.bottomAnchor.constraint(equalTo:contentView.bottomAnchor),
                imageParagraph.leftAnchor.constraint(equalTo:contentView.leftAnchor),
                imageParagraph.rightAnchor.constraint(equalTo:contentView.rightAnchor),
            ])
    }
    
    func configureCell(){
        guard let imageCover = self.imageCover else{return}
        self.imageParagraph.kf.setImage(with: imageCover.getOriginalURL, placeholder: imageCover.getPreviewImage, options: [.transition(.fade(0.5)), .cacheOriginalImage])
    }
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
}
