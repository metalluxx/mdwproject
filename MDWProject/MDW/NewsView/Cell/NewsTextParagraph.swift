//
//  NewsParagraphText.swift
//  MDW
//
//  Created by Metalluxx on 28/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import MDWDomain

class NewsTextParagraph:UITableViewCell{
    static var reuseIdentifier = "NEWS_PARAGRAPH_CELL"
    
    private var textParagraph = UILabel()
    private var titleParagraph = UILabel()
    
    var block:MDWMaterialBlock?
    
    private var arrayConstrainIfHaveTitle:[NSLayoutConstraint]!
    private var arrayConstrainIfDontHaveTitle:[NSLayoutConstraint]!
    
    override init(style:UITableViewCell.CellStyle, reuseIdentifier:String?){
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        setupUI()
    }
    
    private func setupUI(){
        self.selectionStyle = .none

        titleParagraph.numberOfLines = 0
        titleParagraph.lineBreakMode = .byWordWrapping
        titleParagraph.textColor = .black

        textParagraph.numberOfLines = 0
        textParagraph.lineBreakMode = .byWordWrapping
        textParagraph.textColor = .black

        titleParagraph.preferredMaxLayoutWidth = UIScreen.main.bounds.size.width - 40
        textParagraph.preferredMaxLayoutWidth = UIScreen.main.bounds.size.width - 40
 
        titleParagraph.translatesAutoresizingMaskIntoConstraints = false
        textParagraph.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.insertSubview(titleParagraph, at: 0)
        contentView.insertSubview(textParagraph, at: 0)
        
        arrayConstrainIfHaveTitle = [
            textParagraph.topAnchor.constraint(equalTo:titleParagraph.bottomAnchor, constant:25),
            textParagraph.leftAnchor.constraint(equalTo:contentView.leftAnchor, constant:20),
            textParagraph.rightAnchor.constraint(equalTo:contentView.rightAnchor, constant:-20),
            textParagraph.bottomAnchor.constraint(equalTo:contentView.bottomAnchor, constant:-15),
        ]
        
        arrayConstrainIfDontHaveTitle = [
            textParagraph.topAnchor.constraint(equalTo:contentView.topAnchor, constant:15),
            textParagraph.leftAnchor.constraint(equalTo:contentView.leftAnchor, constant:20),
            textParagraph.rightAnchor.constraint(equalTo:contentView.rightAnchor, constant:-20),
            textParagraph.bottomAnchor.constraint(equalTo:contentView.bottomAnchor, constant:-15),
        ]
        
        NSLayoutConstraint.activate([titleParagraph.topAnchor.constraint(equalTo:contentView.topAnchor, constant:15),
                                     titleParagraph.rightAnchor.constraint(equalTo:contentView.rightAnchor, constant:-20),
                                     titleParagraph.leftAnchor.constraint(equalTo:contentView.leftAnchor, constant:20)])
        
        NSLayoutConstraint.activate(arrayConstrainIfDontHaveTitle)
    }
    
    func configureCell(){
        if let block = block{
            //MARK: - Text block
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            textParagraph.attributedText = NSAttributedString(string:block.content.text ?? "", attributes:
                [
                    NSAttributedString.Key.paragraphStyle:paragraphStyle,
                    NSAttributedString.Key.font:MDWConstant.normalFont(ofSize:17)
                ])
            
            //MARK: - Title block
            if let titleBlock = block.content.title{
                NSLayoutConstraint.deactivate(arrayConstrainIfDontHaveTitle)
                NSLayoutConstraint.activate(arrayConstrainIfHaveTitle)
                
                titleParagraph.attributedText = NSAttributedString(string:titleBlock , attributes:
                    [
                        NSAttributedString.Key.paragraphStyle:paragraphStyle,
                        NSAttributedString.Key.font:MDWConstant.boldFont(ofSize:17)
                    ])
            } else{
                NSLayoutConstraint.deactivate(arrayConstrainIfHaveTitle)
                NSLayoutConstraint.activate(arrayConstrainIfDontHaveTitle)
            }
        } else{
            titleParagraph.text = ""
            textParagraph.text = ""
        }
        layoutIfNeeded()
    }
    
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
}
