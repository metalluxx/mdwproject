//
//  MainViewCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 16/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit
import ApplicationService

class MainViewCoordinatorDepricated:BaseCoordinator,Presentable{
    weak var currentPresentor:CoordinatorViewController?
    deinit{
        print("Dealloc \(self)")
    }
    override func start(){
        DispatchQueue.main.async{
            let viewController = MDWTabController()
            self.currentPresentor = viewController
            self.currentPresentor?.manageCoordinator = self
            self.router.setRootModule(self,transitionOption:[.transitionFlipFromRight, .curveEaseInOut])
        }
    }
}
