//
//  InfoButtonCell.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit

public class MDWLargeButton:UIControl{
    private var label = UILabel()
    
    public var titleText:String?{
        set{ label.text = newValue }
        get{ return label.text }
    }

    override public init(frame:CGRect){
        super.init(frame:frame)
        backgroundColor = .gray

        label.text = ""
        label.font = .systemFont(ofSize: 17)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo:topAnchor, constant:10),
            label.leftAnchor.constraint(equalTo:leftAnchor, constant:10),
            label.rightAnchor.constraint(equalTo:rightAnchor, constant:-10),
            label.heightAnchor.constraint(equalToConstant:20),
            ])
    }
    
    required public init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
}
