
//
//  ErrorAlertNotificationView.swift
//  MDW
//
//  Created by Metalluxx on 06/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class ErrorAlertNotificationView:UIView {
    
    public enum TypeNotification {
        case error, alert, none
        
        var getBackgroundColor:UIColor{
            switch self{
            case .alert: return .green
            case .error: return .init(red:1, green:87/255, blue:48/255, alpha:1)
            default: return .white
            }
        }
        var getTextColor:UIColor{
            switch self {
            case .alert: return .black
            case .error: return .white
            default: return .white
            }
        }
    }
    
    private let label = UILabel()
    
    required init(_ info:String, type:TypeNotification) {
        super.init(frame:CGRect.zero)

        backgroundColor = .clear
        label.layer.cornerRadius = 10
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 15
        layer.shadowOpacity = 0.8
        
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = .white
        label.textAlignment = .center
        
        label.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        insertSubview(label, at: 0)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            label.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            ])
        setErrorInformation(info, type:type)
    }
    
    public func setErrorInformation(_ info:String, type:TypeNotification){
        label.backgroundColor = type.getBackgroundColor
        label.textColor = type.getTextColor
        label.text = info
    }

    required public init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
    
    @discardableResult
    static public func showingAlert(in view:UIView, info:String, type:TypeNotification) -> ErrorAlertNotificationView {
        let viewController = ErrorAlertNotificationView(info, type:type)
        
        UIView.transition(with:view, duration:0.25, options:.transitionCrossDissolve, animations:{
            view.addSubview(viewController)
        })
        
        NSLayoutConstraint.activate([
            viewController.topAnchor.constraint(equalTo:view.layoutMarginsGuide.topAnchor),
            viewController.heightAnchor.constraint(equalToConstant:60),
            viewController.leftAnchor.constraint(equalTo:view.leftAnchor),
            viewController.rightAnchor.constraint(equalTo:view.rightAnchor),
            ])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            UIView.transition(with:view, duration:0.25, options:.transitionCrossDissolve, animations:{
                viewController.removeFromSuperview()
            })
        }
        return viewController
    }
}
