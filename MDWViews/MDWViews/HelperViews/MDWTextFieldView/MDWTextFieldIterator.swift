//
//  sd.swift
//  MDWViews
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWTextFieldIterator:IteratorProtocol {
    public typealias Element = MDWTextFieldView
    private var textFields:[Element]
    public var value:Element?{ return textFields.first }
    
    public init(textFields:[Element]){
        self.textFields = textFields
    }
    
    @discardableResult
    public mutating func next()->Element?{
        defer{
            if !textFields.isEmpty{ textFields.removeFirst() }
        }
        return textFields.first
    }
}
