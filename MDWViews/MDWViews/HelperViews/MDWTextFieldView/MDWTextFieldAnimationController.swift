//
//  MDWTextFieldAnimationController.swift
//  MDW
//
//  Created by Metalluxx on 20/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class MDWTextFieldAnimationController:NSObject,UITextFieldDelegate{

    public var textFields = [MDWTextFieldView]()
    public var returnController:MDWTextFieldReturnController?
    
    public func textFieldDidBeginEditing(_ textField:UITextField){

        if (textField.text?.isEmpty)! {
            for tfFromArray in textFields{
                if tfFromArray.fieldText.tag == textField.tag{
                    tfFromArray.animateLabelToUp()
                    break
                }
            }
        }
    }
    
    public func textFieldDidEndEditing(_ textField:UITextField){
        if (textField.text?.isEmpty)! {
            for tfFromArray in textFields{
                if tfFromArray.fieldText.tag == textField.tag{
                    tfFromArray.animateLabelToDown()
                    break
                }
            }
        }
    }
    
    public func textFieldShouldReturn(_ textField:UITextField)->Bool{
        return returnController?.textFieldShouldReturn(textField) ?? true
    }
}
