//
//  MDWTextFieldView.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class MDWTextFieldView:UIView{
    public let fieldText = UITextField()
    public let labelText = UILabel()
    public let line = UIView()
    
    public var animationController:MDWTextFieldAnimationController?{
        set{self.fieldText.delegate = newValue}
        get{return self.fieldText.delegate as? MDWTextFieldAnimationController}
    }
    
    override public var tag:Int{
        didSet{
            fieldText.tag = tag
            labelText.tag = tag
        }
    }
    
    public var lineColor:UIColor?{
        set{ line.backgroundColor = newValue }
        get{ return line.backgroundColor }
    }
    
    private var referenceFrame = CGRect.zero
    
    override public init(frame:CGRect){
        super.init(frame:frame)
        
        tag = Int.random(in:Int.min...Int.max)
        
        line.backgroundColor = .gray
        
        fieldText.placeholder = ""
        fieldText.clipsToBounds = false
        fieldText.keyboardType = .default
        fieldText.textColor = .black
        fieldText.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        fieldText.font = .systemFont(ofSize: 24)

        labelText.text = ""
        labelText.font = .systemFont(ofSize: 24)
        labelText.textColor = .black
        labelText.textAlignment = .left
        labelText.sizeToFit()
        
        line.translatesAutoresizingMaskIntoConstraints = false
        fieldText.translatesAutoresizingMaskIntoConstraints = false
        labelText.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(line, at: 0)
        insertSubview(fieldText, at: 0)
        insertSubview(labelText, at: 1)
        
        NSLayoutConstraint.activate([
            line.bottomAnchor.constraint(equalTo:bottomAnchor),
            line.heightAnchor.constraint(equalToConstant:1),
            line.leftAnchor.constraint(equalTo:leftAnchor),
            line.rightAnchor.constraint(equalTo:rightAnchor),
            
            fieldText.bottomAnchor.constraint(equalTo:line.topAnchor, constant:-8),
            fieldText.heightAnchor.constraint(equalToConstant:24),
            fieldText.leftAnchor.constraint(equalTo:leftAnchor),
            fieldText.rightAnchor.constraint(equalTo:rightAnchor),
            
            labelText.topAnchor.constraint(equalTo:fieldText.topAnchor),
            labelText.leftAnchor.constraint(equalTo:fieldText.leftAnchor, constant:2),
            ])
    }
    
    required public init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
    
    public func animateLabelToUp(){
        referenceFrame = labelText.frame
        UIView.animate(withDuration:0.2){
            self.labelText.transform = CGAffineTransform(scaleX:self.scaleMultiplier, y:self.scaleMultiplier)
        }
        UIView.animate(withDuration:0.2){
            self.labelText.frame.origin.x = 0
            self.labelText.frame.origin.y -= 25
        }
    }
    
    public func animateLabelToDown(){
        UIView.animate(withDuration:0.2){
            self.labelText.transform = CGAffineTransform(scaleX:self.scaleMultiplier, y:self.scaleMultiplier)
        }
        UIView.animate(withDuration:0.2){
            self.labelText.frame.origin.x = self.referenceFrame.origin.x
            self.labelText.frame.origin.y = self.referenceFrame.origin.y
        }
    }

    private var scaleMultiplier:CGFloat{
        let ret:CGFloat = labelText.frame.size.height
        return 15 / ret
    }
}
