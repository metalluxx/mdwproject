//
//  MDWTextFieldReturnController.swift
//  MDW
//
//  Created by Metalluxx on 23/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol MDWTextFieldReturnDelegte{
    var textFields:[MDWTextFieldView]? {get set}
    func textFieldShouldReturn(_ textField:UITextField)->Bool
}

public class MDWTextFieldReturnController:MDWTextFieldReturnDelegte{
    public var textFields:[MDWTextFieldView]?
    deinit{ textFields = nil }
    public var alwaysEndEditing = false
    
    required public init(withArray array:[MDWTextFieldView]){
        self.textFields = array
    }
    
    convenience public init(with animController:MDWTextFieldAnimationController){
        self.init(withArray:animController.textFields)
    }
    
    convenience public init(with animController:MDWTextFieldAnimationController, alwaysReturn:Bool){
        self.init(with:animController)
        alwaysEndEditing = alwaysReturn
    }
    
    public func textFieldShouldReturn(_ textField:UITextField)->Bool{
        var iterator  = makeIterator()
        while iterator.value?.tag != textField.tag{
            iterator.next()
        }
        
        if alwaysEndEditing {
            iterator.value?.endEditing(true)
        } else{
        
        if let nextTextField = iterator.next(){
            guard let value = iterator.value else{
                nextTextField.endEditing(true);
                return false
            }
            value.fieldText.becomeFirstResponder()
        }
    }
        return true
    }
}

extension MDWTextFieldReturnController:Sequence{
    public func makeIterator() -> MDWTextFieldIterator{
        return MDWTextFieldIterator(textFields:textFields!)
    }
}
