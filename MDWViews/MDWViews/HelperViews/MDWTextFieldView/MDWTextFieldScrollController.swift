
//
//  MDWTextFieldScrollController.swift
//  MDW
//
//  Created by Metalluxx on 08/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class MDWTextFieldScrollController:MDWTextFieldAnimationController{
    public weak var contentScrollView:UIScrollView?
    private(set) weak var currentTextField:MDWTextFieldView?
    
    private var rectKeyboard = CGRect.zero
    
    override public init(){
        super.init()
        NotificationCenter.default.addObserver(self, selector:#selector(insertRectangleKeyboardFromNotification) , name:UIResponder.keyboardWillShowNotification, object:nil)
    }
    
    @objc private func insertRectangleKeyboardFromNotification(notification:Notification){
        rectKeyboard = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    }
    
    private func refreshOffset(){
        guard let currentTextField = currentTextField,  let scrollView = contentScrollView else{ return }
        (rectKeyboard.origin.y < (currentTextField.frame.origin.y + currentTextField.frame.height + 100)) ?
            scrollView.contentOffset = CGPoint(x:0, y:(currentTextField.frame.origin.y + currentTextField.frame.height - rectKeyboard.origin.y + 100)) :
            (scrollView.contentOffset = .zero)
    }
    
    override public func textFieldDidBeginEditing(_ textField:UITextField){
        super.textFieldDidBeginEditing(textField)
        guard let textField = textField.superview as? MDWTextFieldView else{
                currentTextField = nil
                return
        }
        currentTextField = textField
        refreshOffset()
    }
    
    override public func textFieldDidEndEditing(_ textField:UITextField){
        super.textFieldDidEndEditing(textField)
        currentTextField?.fieldText.resignFirstResponder()
        currentTextField = nil
    }
    
    override public func textFieldShouldReturn(_ textField:UITextField)->Bool{
        return returnController?.textFieldShouldReturn(textField) ?? true
    }
}
