//
//  ItemEntity.swift
//  MDW
//
//  Created by Metalluxx on 19/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol MDWTabBarItemFieldEntity{
    var image:UIImage?{get set}
    var selectedImage:UIImage?{get}
    var title:String{get set}
}

extension MDWTabBarItemFieldEntity{
    public var selectedImage:UIImage?{return nil}
}

public struct MDWTabBarItemField:MDWTabBarItemFieldEntity,Equatable{
    public var image:UIImage?
    public var selectedImage:UIImage?{
        return image?.imageWithColor(color: .black)
    }
    public var title:String
    public var id:UInt32 = 0
    
    public init(image:UIImage?,title:String){
        self.image = image
        self.title = title
        self.id = arc4random()
    }
    public static func == (left:MDWTabBarItemField,right:MDWTabBarItemField) -> Bool{
        return left.id == right.id
    }
}
