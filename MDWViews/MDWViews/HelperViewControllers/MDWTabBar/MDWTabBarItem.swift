//
//  MDWTabBarItem.swift
//  MDW
//
//  Created by Metalluxx on 09/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class MDWTabBarItem:UIControl{
    public weak var delegate:MDWTabBarSelectDelegate?
    
    public var itemEntity:MDWTabBarItemField?{
        didSet{
            label.text = itemEntity?.title
            imageView.image = itemEntity?.image
        }
    }

    public override var isSelected: Bool{
        didSet{
            if isSelected{
                label.textColor = .black
                imageView.image = itemEntity?.selectedImage ?? nil
            } else{
                label.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
                imageView.image = itemEntity?.image ?? nil
            }
        }
    }
    
    private var imageView = UIImageView()
    private var label = UILabel()
    
    override public init(frame:CGRect){
        super.init(frame:CGRect.zero)
        addTarget(self, action: #selector(tappedItem), for: .touchUpInside)
        
        imageView.contentMode = .scaleAspectFit
        label.font = .systemFont(ofSize: 13)
        label.textAlignment = .center
        label.numberOfLines = 1
        label.text = "Empty"
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(imageView, at: 0)
        insertSubview(label, at: 0)
        
        NSLayoutConstraint.activate([
            label.heightAnchor.constraint(equalToConstant:15),
            label.leftAnchor.constraint(equalTo:leftAnchor),
            label.rightAnchor.constraint(equalTo:rightAnchor),
            label.bottomAnchor.constraint(equalTo:bottomAnchor, constant:-4),
            
            imageView.bottomAnchor.constraint(equalTo:label.topAnchor, constant:-7),
            imageView.leftAnchor.constraint(equalTo:leftAnchor),
            imageView.rightAnchor.constraint(equalTo:rightAnchor),
            imageView.topAnchor.constraint(equalTo:topAnchor, constant:8)
            ])
    }
    
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
    
    @objc private func tappedItem(){
        guard let delegate = delegate else{
            print("Dont have tabBarController instance in \(self)")
            return
        }
        delegate.selectedItem(sender:self)
    }
    
    public func setState(isSelected:Bool){
        self.isSelected = isSelected
    }
}




