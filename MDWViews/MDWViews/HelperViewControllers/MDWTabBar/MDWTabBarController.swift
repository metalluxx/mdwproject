//
//  MDWTabBarController.swift
//  MDW
//
//  Created by Metalluxx on 10/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationService

public class MDWTabBarController:CoordinatorViewController{
    public weak var manageCoordinator: BaseCoordinator?

    private var contentView = UIView()
    private var tabBar = MDWTabBar()

    public var viewControllers = [MDWTabBarContentVC](){
        didSet{
            var arrayItems = [MDWTabBarItem]()
            for viewController in viewControllers{
                let tbItem = MDWTabBarItem()
                tbItem.delegate = tabBar
                tbItem.itemEntity = viewController.mdwTabBarItem
                arrayItems.append(tbItem)
                
                guard let viewController = viewController as? CoordinatorViewController else{continue}
                viewController.manageCoordinator = self.manageCoordinator
            }
            self.tabBar.tabBarItems = arrayItems
            currentViewController = viewControllers[0]
        }
    }
    public private(set) var currentViewController:MDWTabBarContentVC?{
        willSet{switchViewController(from:currentViewController, to:newValue)}
    }
    override public func viewDidLoad(){
        super.viewDidLoad()
        tabBar.delegate = self
        tabBar.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(tabBar, at:0)
        view.insertSubview(contentView, at:0)
        
        NSLayoutConstraint.activate([
            tabBar.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            tabBar.leftAnchor.constraint(equalTo:view.leftAnchor),
            tabBar.rightAnchor.constraint(equalTo:view.rightAnchor),
            tabBar.topAnchor.constraint(equalTo: tabBar.safeAreaLayoutGuide.bottomAnchor, constant: -60),
            
            contentView.topAnchor.constraint(equalTo:view.topAnchor),
            contentView.leftAnchor.constraint(equalTo:view.leftAnchor),
            contentView.rightAnchor.constraint(equalTo:view.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo:tabBar.topAnchor),
            ])
    }
    
    private func switchViewController(from:UIViewController?, to:UIViewController?){
        if let from = from, let to = to{
            UIView.transition(from: from.view, to: to.view, duration: 0.1, options: .transitionCrossDissolve)
        }
        if from != nil{
            from!.willMove(toParent:nil)
            from!.view.removeFromSuperview()
            from!.removeFromParent()
        }
        if to != nil{
            addChild(to!)
            to!.view.translatesAutoresizingMaskIntoConstraints = false
            contentView.insertSubview(to!.view, at:0)
            NSLayoutConstraint.activate([
                to!.view.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                to!.view.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
                to!.view.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
                to!.view.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
                ])
            to!.didMove(toParent:self)
        }
    }
}


extension MDWTabBarController:MDWTabBarSelectDelegate{
    public func selectedItem(sender: MDWTabBarItem){
        for viewController in viewControllers{
            guard let item = sender.itemEntity else{continue}
            if viewController.mdwTabBarItem == item{
                currentViewController = viewController
            }
        }
    }
}
