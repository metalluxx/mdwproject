//
//  Protocols .swift
//  MDW
//
//  Created by Metalluxx on 11/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public typealias MDWTabBarContentVC = UIViewController & MDWTabBarItemContainer

#warning("Rewrite the selection protocol")
public protocol MDWTabBarSelectDelegate:class{
    func selectedItem(sender:MDWTabBarItem)
}
public protocol MDWTabBarItemContainer where Self:UIViewController{
    var mdwTabBarItem:MDWTabBarItemField{get set}
}







