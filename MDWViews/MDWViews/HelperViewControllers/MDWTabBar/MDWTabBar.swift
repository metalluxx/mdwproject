//
//  MDWTabBar.swift
//  MDW
//
//  Created by Metalluxx on 09/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public class MDWTabBar:UIView{
    public weak var delegate:MDWTabBarSelectDelegate?
    private var stackView = UIStackView()
    private var separatorView = UIView()
    
    public var separatorColor:UIColor?{
        set{separatorView.backgroundColor = newValue}
        get{return separatorView.backgroundColor}
    }
    public var tabBarItems = [MDWTabBarItem](){
        willSet{
            for item in tabBarItems{
                stackView.removeArrangedSubview(item)
            }
        }
        didSet{
            for (index, item) in tabBarItems.enumerated(){
                item.setState(isSelected:index == 0)
                stackView.insertArrangedSubview(item, at:index)
            }
        }
    }
    override public init(frame:CGRect){
        super.init(frame:frame)
        backgroundColor = .white
        separatorColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .horizontal
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(stackView, at: 0)
        insertSubview(separatorView, at: 1)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo:topAnchor),
            stackView.leftAnchor.constraint(equalTo:leftAnchor),
            stackView.rightAnchor.constraint(equalTo:rightAnchor),
            stackView.bottomAnchor.constraint(equalTo:safeAreaLayoutGuide.bottomAnchor),
            
            separatorView.topAnchor.constraint(equalTo: topAnchor),
            separatorView.leftAnchor.constraint(equalTo: leftAnchor),
            separatorView.rightAnchor.constraint(equalTo: rightAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 2),
            ])
    }
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
    }
}

extension MDWTabBar:MDWTabBarSelectDelegate{
    public func selectedItem(sender:MDWTabBarItem){
        for item in tabBarItems{
            item.setState(isSelected:item == sender)
        }
        delegate?.selectedItem(sender:sender)
    }
}

